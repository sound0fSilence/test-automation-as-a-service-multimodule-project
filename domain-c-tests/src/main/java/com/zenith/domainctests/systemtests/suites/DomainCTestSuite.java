package com.zenith.domainctests.systemtests.suites;

import com.zenith.domainctests.systemtests.debit.authorization.AuthorizationValidationTests;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses(AuthorizationValidationTests.class)
@IncludeClassNamePatterns(".*Tests")
public class DomainCTestSuite {

}
