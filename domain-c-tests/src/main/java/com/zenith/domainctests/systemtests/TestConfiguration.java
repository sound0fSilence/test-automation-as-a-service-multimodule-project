package com.zenith.coredomaintests.systemtests;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.zenith.*"})
@ConfigurationPropertiesScan(basePackages = "com.zenith.*")
@EnableAutoConfiguration
public class TestConfiguration {

}
