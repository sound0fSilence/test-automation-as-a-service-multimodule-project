package com.zenith.coredomaintests.systemtests;

import com.common.BaseTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = TestConfiguration.class)
public abstract class AbstractTest extends BaseTest {

}
