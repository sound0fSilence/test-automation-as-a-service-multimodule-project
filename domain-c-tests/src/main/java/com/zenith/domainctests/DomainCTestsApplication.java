package com.zenith.domainctests;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomainCTestsApplication {

    @Test
    public static void main(String[] args) {

        SpringApplication.run(DomainCTestsApplication.class, args);
    }

}
