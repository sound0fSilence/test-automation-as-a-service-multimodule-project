package com.zenith.domainctests.core.config.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Entity {

    private String id;
    private String name;
    private String lastName;
}
