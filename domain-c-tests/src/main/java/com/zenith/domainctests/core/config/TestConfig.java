package com.zenith.domainctests.core.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Getter
    @Autowired
    private EntityConfig entityConfig;
}
