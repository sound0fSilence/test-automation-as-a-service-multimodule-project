package com.zenith.domainctests.core.annotations.tags;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import org.junit.jupiter.api.Tag;

@Retention(RUNTIME)
@Tag("tag_example")
public @interface ExampleTag {

}
