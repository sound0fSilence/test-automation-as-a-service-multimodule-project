package com.zenith.domainctests.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("environment-config")
public class EnvironmentConfig {

    private String backend;
    private String wireMockHost;
}
