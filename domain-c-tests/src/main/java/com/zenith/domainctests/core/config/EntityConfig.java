package com.zenith.domainctests.core.config;

import com.common.exceptions.TAFRuntimeException;
import com.common.factory.CommonPropertySourceFactory;

import com.zenith.domainctests.core.config.models.Entity;
import java.util.List;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Log4j2
@PropertySource(
        value = "classpath:static_config/entities.yml",
        factory = CommonPropertySourceFactory.class
)
@EnableConfigurationProperties(EntityConfig.class)
@Configuration
@ConfigurationProperties("entities-configuration")
public class EntityConfig {

    private List<Entity> entities;

    public Entity getDefaultEntity() {

        var entity = entities
                .stream()
                .findFirst()
                .orElseThrow(() -> new TAFRuntimeException("No entity available"));
        log.info("Selected entity:{}", entity.getName());

        return entity;
    }
}
