package com.zenith.domainctests.domain.dto.microservice_name.requests;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ExampleRequestDto {

    private String firstName;
    private String lastName;
    private String field3;
    private String field4;
}
