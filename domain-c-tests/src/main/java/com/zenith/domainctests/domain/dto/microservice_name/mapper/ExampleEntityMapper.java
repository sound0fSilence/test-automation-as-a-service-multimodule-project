package com.zenith.domainctests.domain.dto.microservice_name.mapper;

import com.zenith.domainctests.domain.dto.microservice_name.requests.ExampleRequestDto;
import com.zenith.domainctests.domain.dto.microservice_name.responses.ExampleResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ExampleEntityMapper {

    ExampleEntityMapper INSTANCE = Mappers.getMapper(ExampleEntityMapper.class);

    @Mapping(target = "firstName", source = "name")
    ExampleRequestDto responseToRequest(ExampleResponseDto response);

    @Mapping(target = "name", source = "firstName")
    ExampleResponseDto requestToResponse(ExampleRequestDto request);
}