package com.zenith.domainctests.domain.dto.microservice_name.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ExampleResponseDto {

    private String name;
    private String lastName;
    private String field3;
    private String field4;
}
