package com.zenith.domainctests.engine.jdbc.services;

import com.zenith.domainctests.core.config.models.Entity;
import com.zenith.domainctests.engine.jdbc.mapper.ExampleMapper;
import com.zenith.domainctests.engine.jdbc.queries.Queries;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ExampleSqlService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Entity getEntityById(String entityId) {
        log.info("Get entity from DB with id: {}", entityId);
        return jdbcTemplate.queryForObject(String.format(Queries.GET_ENTITY_BY_ID, entityId), new ExampleMapper());
    }
}
