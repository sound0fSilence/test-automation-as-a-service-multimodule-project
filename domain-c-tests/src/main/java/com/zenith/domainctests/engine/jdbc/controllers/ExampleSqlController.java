package com.zenith.domainctests.engine.jdbc.controllers;

import com.zenith.domainctests.core.config.models.Entity;
import com.zenith.domainctests.engine.jdbc.services.ExampleSqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ExampleSqlController {

    @Autowired
    private ExampleSqlService entitySqlService;


    public Entity getEntityById(String entityId) {
        return entitySqlService.getEntityById(entityId);
    }

}
