package com.zenith.domainctests.engine.ws.controllers.servicename.interfaces;

import com.common.utils.restassuredwrapper.HttpResponseWrapper;
import com.zenith.domainctests.domain.dto.microservice_name.requests.ExampleRequestDto;
import org.junit.jupiter.api.DisplayName;

public interface ExampleController {

    HttpResponseWrapper getEntityById(String entityId, String token, int status);

    @DisplayName("Send get entity by ID request")
    HttpResponseWrapper getEntityById(String entityId);

    HttpResponseWrapper updateEntity(String exampleId, ExampleRequestDto example, String token, int statusCode);

    HttpResponseWrapper createEntityProfile(ExampleRequestDto entity, String token, int expectedCode);
}
