package com.zenith.domainctests.engine.ws.controllers.servicename.implementations;

import com.common.parser.JsonHandling;
import com.common.utils.restassuredwrapper.HttpResponseWrapper;
import com.zenith.domainctests.engine.ws.restclient.servicename.interfaces.ExampleRestClient;
import com.zenith.domainctests.domain.dto.microservice_name.requests.ExampleRequestDto;
import com.zenith.domainctests.engine.ws.controllers.servicename.interfaces.ExampleController;
import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.DisplayName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Log4j2
@Controller
public class ExampleControllerImp implements ExampleController {

    @Autowired
    private ExampleRestClient exampleClient;

    @DisplayName("Send get entity by ID request")
    @Override
    public HttpResponseWrapper getEntityById(String entityId, String token, int status) {
        log.info("Send get entity by Id: {} request with access token", entityId);

        return sendGetExampleByIdRequest(entityId, token)
                .expectStatusCode("Get entity request returned inappropriate status code", status);
    }

    @DisplayName("Send get entity by ID request")
    @Override
    public HttpResponseWrapper getEntityById(String entityId) {
        log.info("Send get entity by Id: {} request without access token", entityId);

        return sendGetExampleByIdRequest(entityId, null);
    }

    @DisplayName("Send update entity request")
    @Override
    public HttpResponseWrapper updateEntity(String entityId, ExampleRequestDto entity, String token, int statusCode) {
        log.info("Send update entity request with access token");

        return sendUpdateEntityRequest(entityId, entity, token)
                .expectStatusCode("Update entity request returned inappropriate status code", statusCode);
    }

    @Override
    @Step("Send create entity response with body: {entity}")
    public HttpResponseWrapper createEntityProfile(ExampleRequestDto entity, String token, int expectedCode) {
        log.info("Create entity with token");

        return sendCreateExampleRequest(token, entity)
                .expectStatusCode("Create entity request returned inappropriate status code", expectedCode);
    }

    private HttpResponseWrapper sendUpdateEntityRequest(String entityId, ExampleRequestDto entity, String token) {
        return exampleClient.sendUpdateExampleRequest(entityId, JsonHandling.toJson(entity), token);
    }

    private HttpResponseWrapper sendGetExampleByIdRequest(String entityId, String token) {
        return exampleClient.sendGetExampleByIdRequest(entityId, token);
    }

    private HttpResponseWrapper sendCreateExampleRequest(String token, ExampleRequestDto entityRequestDto) {
        return exampleClient.sendCreateExampleRequest(token, JsonHandling.toJson(entityRequestDto));
    }
}
