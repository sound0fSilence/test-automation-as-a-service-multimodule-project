package com.zenith.domainctests.engine.ws.restclient.servicename.implementations;

import com.common.annotations.RestClient;
import com.common.factory.RestClientFactory;
import com.common.utils.restassuredwrapper.HttpResponseWrapper;
import com.common.utils.restassuredwrapper.RestClientWrapper;
import com.zenith.domainctests.core.config.EnvironmentConfig;
import com.zenith.domainctests.engine.ws.restclient.servicename.interfaces.ExampleRestClient;
import org.springframework.beans.factory.annotation.Autowired;

@RestClient
public class ExampleRestClientImpl implements ExampleRestClient {

    private static final String EXAMPLE_BASE_URL = "/example";
    private static final String ENTITY_URL = EXAMPLE_BASE_URL + "/{exampleId}";
    @Autowired
    private EnvironmentConfig environmentConfig;

    @Override
    public RestClientWrapper getClient() {
        return RestClientFactory.getClient(environmentConfig.getBackend());
    }

    @Override
    public HttpResponseWrapper sendGetExampleByIdRequest(String entityId, String token) {

        return getClient()
                .addBearerToken(token)
                .get(ENTITY_URL, entityId);
    }

    @Override
    public HttpResponseWrapper sendUpdateExampleRequest(String entityId, String body, String token) {
        return getClient()
                .addBearerToken(token)
                .body(body)
                .put(ENTITY_URL, entityId);
    }

    @Override
    public HttpResponseWrapper sendCreateExampleRequest(String token, String body) {
        return getClient()
                .addBearerToken(token)
                .body(body)
                .post(EXAMPLE_BASE_URL);
    }
}
