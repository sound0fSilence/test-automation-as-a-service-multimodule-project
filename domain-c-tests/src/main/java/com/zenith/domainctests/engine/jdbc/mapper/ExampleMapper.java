package com.zenith.domainctests.engine.jdbc.mapper;

import com.zenith.domainctests.core.config.models.Entity;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ExampleMapper implements RowMapper<Entity> {

    @Override
    public Entity mapRow(ResultSet rs, int rowNum) throws SQLException {
        return Entity
                .builder()
                .id(rs.getString("id"))
                .build();
    }
}
