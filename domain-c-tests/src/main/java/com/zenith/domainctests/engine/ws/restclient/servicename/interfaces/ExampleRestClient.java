package com.zenith.domainctests.engine.ws.restclient.servicename.interfaces;

import com.common.utils.restassuredwrapper.HttpResponseWrapper;
import com.zenith.domainctests.engine.ws.restclient.ApiClient;

public interface ExampleRestClient extends ApiClient {

    HttpResponseWrapper sendGetExampleByIdRequest(String entityId, String token);

    HttpResponseWrapper sendUpdateExampleRequest(String entityId, String body, String token);

    HttpResponseWrapper sendCreateExampleRequest(String token, String body);
}
