package com.zenith.domainctests.engine.jdbc.queries;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class Queries {

    public static final String GET_ENTITY_BY_ID = "SELECT * FROM table WHERE id = '%s'";
}
