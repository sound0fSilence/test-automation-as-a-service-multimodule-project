package com.zenith.domainctests.engine.ws.restclient;

import com.common.utils.restassuredwrapper.RestClientWrapper;

public interface ApiClient {

    RestClientWrapper getClient();

}
