package com.zenith.testsrunner.dtos;

public enum Domains {
    DOMAIN_A,
    DOMAIN_B,
    DOMAIN_C
}
