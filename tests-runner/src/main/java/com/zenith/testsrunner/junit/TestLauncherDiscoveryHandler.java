package com.zenith.testsrunner.junit;

import static org.junit.platform.launcher.TagFilter.includeTags;

import com.zenith.domainatests.DomainATestsApplication;
import com.zenith.domainbtests.systemtests.suites.DomainBTestSuite;
import com.zenith.domainctests.systemtests.suites.DomainCTestSuite;
import com.zenith.testsrunner.dtos.Domains;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.LauncherSession;
import org.junit.platform.launcher.TestPlan;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestLauncherDiscoveryHandler {

    public static LauncherDiscoveryRequest getLauncherDiscoveryRequest(Domains domain, String tags) {
        return LauncherDiscoveryRequestBuilder
                .request()
                .selectors(
                        DiscoverySelectors.selectClass(getDomainTestSuite(domain))
                )
                .filters(
                        includeTags(tags)
                )
                .configurationParameters(Map.of(
                        "junit.jupiter.execution.parallel.enabled", "true",
                        "junit.jupiter.execution.parallel.mode.default", "concurrent",
                        "junit.jupiter.execution.parallel.mode.classes.default", "concurrent",
                        "junit.jupiter.execution.parallel.config.strategy", "fixed",
                        "junit.jupiter.execution.parallel.config.fixed.parallelism", "4",
                        "junit.jupiter.extensions.autodetection.enabled", "true"))
                .build();
    }

    private static Class<?> getDomainTestSuite(Domains domain) {
        return switch (domain) {
            case DOMAIN_A -> DomainATestsApplication.class;
            case DOMAIN_B -> DomainBTestSuite.class;
            case DOMAIN_C -> DomainCTestSuite.class;
        };
    }

    public static void launchDiscoveredTests(LauncherDiscoveryRequest request) {
        SummaryGeneratingListener listener = new SummaryGeneratingListener();

        try (LauncherSession session = LauncherFactory.openSession()) {
            Launcher launcher = session.getLauncher();
            // Register a listener of your choice
            launcher.registerTestExecutionListeners(listener);
            // Discover tests and build a test plan
            TestPlan testPlan = launcher.discover(request);
            // Execute test plan
            launcher.execute(testPlan);
            // Alternatively, execute the request directly
            launcher.execute(request);
        }

        TestExecutionSummary summary = listener.getSummary();

        log.info("---------------------------------------------------------------------------------------------------");
        log.info("Tests executed {}", summary.getTestsFoundCount());
        log.info("Tests passed {}", summary.getTestsSucceededCount());
        log.info("Tests aborted {}", summary.getTestsAbortedCount());
        log.info("Tests skipped {}", summary.getTestsSkippedCount());
        log.info("Tests failed {}", summary.getTestsFailedCount());
        log.info("Test stared at {}", summary.getTimeStarted());
        log.info("Test finished at {}", summary.getTimeFinished());
        log.info("Test duration is {}",
                TimeUnit.MILLISECONDS.toSeconds(summary.getTimeFinished() - summary.getTimeStarted()));
        log.info("---------------------------------------------------------------------------------------------------");
    }
}
