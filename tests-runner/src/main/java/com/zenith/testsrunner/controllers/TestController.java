package com.zenith.testsrunner.controllers;

import static com.zenith.testsrunner.junit.TestLauncherDiscoveryHandler.getLauncherDiscoveryRequest;
import static com.zenith.testsrunner.junit.TestLauncherDiscoveryHandler.launchDiscoveredTests;

import com.zenith.testsrunner.dtos.TestRequestDto;
import com.zenith.testsrunner.report.allure.AllureReportGenerator;
import lombok.extern.log4j.Log4j2;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class TestController {

    private static final String ALLURE_RESULTS_FOLDER = "allure-results/";
    private static final String ALLURE_REPORT_FOLDER = "allure-report/";

    @PostMapping("/tests")
    public void runTests(@RequestBody @NonNull TestRequestDto requestDto) {

        LauncherDiscoveryRequest request = getLauncherDiscoveryRequest(requestDto.getDomain(), requestDto.getTags());

        launchDiscoveredTests(request);

        AllureReportGenerator.generateAllureReport(ALLURE_RESULTS_FOLDER, ALLURE_REPORT_FOLDER);
    }

}
