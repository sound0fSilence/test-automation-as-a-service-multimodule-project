package com.zenith.testsrunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestsRunnerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestsRunnerApplication.class, args);
    }

}
