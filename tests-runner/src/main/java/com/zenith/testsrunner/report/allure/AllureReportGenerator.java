package com.zenith.testsrunner.report.allure;

import io.qameta.allure.ConfigurationBuilder;
import io.qameta.allure.DummyReportGenerator;
import io.qameta.allure.Extension;
import io.qameta.allure.ReportGenerator;
import io.qameta.allure.allure1.Allure1Plugin;
import io.qameta.allure.allure2.Allure2Plugin;
import io.qameta.allure.category.CategoriesPlugin;
import io.qameta.allure.category.CategoriesTrendPlugin;
import io.qameta.allure.context.FreemarkerContext;
import io.qameta.allure.context.JacksonContext;
import io.qameta.allure.context.MarkdownContext;
import io.qameta.allure.context.RandomUidContext;
import io.qameta.allure.core.AttachmentsPlugin;
import io.qameta.allure.core.Configuration;
import io.qameta.allure.core.LaunchResults;
import io.qameta.allure.core.MarkdownDescriptionsPlugin;
import io.qameta.allure.core.Plugin;
import io.qameta.allure.core.ReportWebPlugin;
import io.qameta.allure.core.TestsResultsPlugin;
import io.qameta.allure.duration.DurationPlugin;
import io.qameta.allure.duration.DurationTrendPlugin;
import io.qameta.allure.environment.Allure1EnvironmentPlugin;
import io.qameta.allure.executor.ExecutorPlugin;
import io.qameta.allure.history.HistoryPlugin;
import io.qameta.allure.history.HistoryTrendPlugin;
import io.qameta.allure.idea.IdeaLinksPlugin;
import io.qameta.allure.influxdb.InfluxDbExportPlugin;
import io.qameta.allure.launch.LaunchPlugin;
import io.qameta.allure.mail.MailPlugin;
import io.qameta.allure.owner.OwnerPlugin;
import io.qameta.allure.plugin.DefaultPluginLoader;
import io.qameta.allure.prometheus.PrometheusExportPlugin;
import io.qameta.allure.retry.RetryPlugin;
import io.qameta.allure.retry.RetryTrendPlugin;
import io.qameta.allure.severity.SeverityPlugin;
import io.qameta.allure.status.StatusChartPlugin;
import io.qameta.allure.suites.SuitesPlugin;
import io.qameta.allure.summary.SummaryPlugin;
import io.qameta.allure.tags.TagsPlugin;
import io.qameta.allure.timeline.TimelinePlugin;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AllureReportGenerator {

    private static final String ALLURE_CONFIG_DIR = "tests-runner/src/main/resources/allure";

    private static final List<Extension> EXTENSIONS = Arrays.asList(
            new JacksonContext(),
            new MarkdownContext(),
            new FreemarkerContext(),
            new RandomUidContext(),
            new MarkdownDescriptionsPlugin(),
            new RetryPlugin(),
            new RetryTrendPlugin(),
            new TagsPlugin(),
            new SeverityPlugin(),
            new OwnerPlugin(),
            new IdeaLinksPlugin(),
            new HistoryPlugin(),
            new HistoryTrendPlugin(),
            new CategoriesPlugin(),
            new CategoriesTrendPlugin(),
            new DurationPlugin(),
            new DurationTrendPlugin(),
            new StatusChartPlugin(),
            new TimelinePlugin(),
            new SuitesPlugin(),
            new TestsResultsPlugin(),
            new AttachmentsPlugin(),
            new MailPlugin(),
            new InfluxDbExportPlugin(),
            new PrometheusExportPlugin(),
            new SummaryPlugin(),
            new ExecutorPlugin(),
            new LaunchPlugin(),
            new Allure1Plugin(),
            new Allure1EnvironmentPlugin(),
            new Allure2Plugin(),
            new ReportWebPlugin() {
                @Override
                public void aggregate(final Configuration configuration,
                        final List<LaunchResults> launchesResults,
                        final Path outputDirectory) throws IOException {
                    writePluginsStatic(configuration, outputDirectory);
                    writeIndexHtml(configuration, outputDirectory);
                    writeStatic(outputDirectory);
                }
            }
    );

    @SneakyThrows
    public static void generateAllureReport(String resultsDir, String reportDir) {

        copyAllureConfigFiles(resultsDir);
        copyHistoryFiles(reportDir, resultsDir);

        final List<Plugin> plugins = loadPlugins();
        log.info("Found {} plugins", plugins.size());
        plugins.forEach(plugin -> log.info(plugin.getConfig().getName()));
        final Configuration configuration = new ConfigurationBuilder()
                .fromExtensions(EXTENSIONS)
                .fromPlugins(plugins)
                .build();
        final ReportGenerator generator = new ReportGenerator(configuration);
        generator.generate(Paths.get(reportDir), Paths.get(resultsDir));
    }

    private static void copyHistoryFiles(String reportDir, String resultDir) {
        File source = new File(reportDir + "history/");
        File dest = new File(resultDir + "history/");
        if (source.exists()) {
            try {
                FileUtils.copyDirectory(source, dest);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void copyAllureConfigFiles(String destination) {

        File source = new File(ALLURE_CONFIG_DIR);
        File dest = new File(destination);
        try {
            FileUtils.copyDirectory(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static List<Plugin> loadPlugins() throws IOException {
        final Optional<Path> optional = Optional.ofNullable(System.getProperty("allure.plugins.directory"))
                .map(Paths::get)
                .filter(Files::isDirectory);
        if (optional.isEmpty()) {
            return Collections.emptyList();
        }
        final Path pluginsDirectory = optional.get();
        log.info("Found plugins directory {}", pluginsDirectory);
        final DefaultPluginLoader loader = new DefaultPluginLoader();
        final ClassLoader classLoader = DummyReportGenerator.class.getClassLoader();
        return Files.list(pluginsDirectory)
                .filter(Files::isDirectory)
                .map(pluginDir -> loader.loadPlugin(classLoader, pluginDir))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
    }
}
