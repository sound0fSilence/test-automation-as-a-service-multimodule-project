package com.zenith.acquisitionandonboardingdomaintests.systemtests;

import com.common.BaseTest;
import com.zenith.domainbtests.systemtests.TestConfiguration;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = TestConfiguration.class)
public abstract class AbstractTest extends BaseTest {

}
