package com.zenith.domainbtests.systemtests.suites;

import com.zenith.domainbtests.systemtests.example.DummyTests;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses(DummyTests.class)
@IncludeClassNamePatterns(".*Tests")
public class DomainBTestSuite {

}
