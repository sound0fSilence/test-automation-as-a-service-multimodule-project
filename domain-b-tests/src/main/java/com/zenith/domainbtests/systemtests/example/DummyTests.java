package com.zenith.domainbtests.systemtests.example;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.zenith.acquisitionandonboardingdomaintests.systemtests.AbstractTest;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Example how to run parametrized tests in one thread and other tests in parallel
 */
@Tag("smoke")
@Log4j2
@Epic("Tests with parallel execution")
public class DummyTests extends AbstractTest {

    @Description("Execution in sequential mode")
    @ParameterizedTest(name = "Parametrized test {index}: input={0}")
    @ValueSource(strings = {"apple", "banana", "cherry"})
    @Execution(ExecutionMode.SAME_THREAD)
    @Tag("ignore")
    public void parametrizedTest(String input) {
        String threadName = Thread.currentThread().getName();
        log.info("parametrizedTest() start => " + threadName);
        // Do something with the input
        assertTrue(input.length() > 0);
    }

    @Description("Execution in parallel mode parametrized test")
    @ParameterizedTest(name = "Another parametrized test {index}: input={0}")
    @ValueSource(strings = {"dog", "cat", "bird"})
    @Execution(ExecutionMode.CONCURRENT)
    public void anotherParametrizedTest(String input) {
        String threadName = Thread.currentThread().getName();
        log.info("anotherParametrizedTest() start => " + threadName);
        // Do something else with the input
        assertTrue(input.length() > 0);
    }

    @Description("Execution in parallel mode non-parametrized demo test one")
    @Test
    @Execution(ExecutionMode.CONCURRENT)
    public void nonParametrizedTestOne() {
        String threadName = Thread.currentThread().getName();
        log.info("nonParametrizedTestOne() start => " + threadName);
        // Do some non-parameterized test logic
        assertTrue(true);
    }

    @Description("Execution in parallel mode non-parametrized demo test two")
    @Test
    @Execution(ExecutionMode.CONCURRENT)
    public void nonParametrizedTestTwo() {
        String threadName = Thread.currentThread().getName();
        log.info("nonParametrizedTestTwo() start => " + threadName);
        // Do some more non-parameterized test logic
        assertTrue(true);
    }

    @Description("Execution in parallel mode non-parametrized demo test three")
    @Test
    @Execution(ExecutionMode.CONCURRENT)
    public void nonParametrizedTestThree() {
        String threadName = Thread.currentThread().getName();
        log.info("nonParametrizedTestThree() start => " + threadName);
        // Do even more non-parameterized test logic
        assertTrue(true);
    }
}
