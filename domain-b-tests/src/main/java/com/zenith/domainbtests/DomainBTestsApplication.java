package com.zenith.domainbtests;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomainBTestsApplication {

    @Test
    public static void main(String[] args) {
        SpringApplication.run(DomainBTestsApplication.class, args);
    }
}
