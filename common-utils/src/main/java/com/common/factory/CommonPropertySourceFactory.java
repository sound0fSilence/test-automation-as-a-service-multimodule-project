package com.common.factory;

import static java.lang.System.identityHashCode;
import static java.util.Objects.isNull;
import static org.springframework.util.StringUtils.hasText;

import java.io.IOException;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public class CommonPropertySourceFactory implements PropertySourceFactory {

    @Override
    public PropertySource<?> createPropertySource(@Nullable String name, @NonNull EncodedResource resource)
            throws IOException {
        return isNull(name) ?
                new YamlPropertySourceLoader()
                        .load(getNameForResource(resource.getResource()), resource.getResource()).get(0) :
                new YamlPropertySourceLoader().load(name, resource.getResource()).get(0);
    }

    private String getNameForResource(Resource resource) {
        var name = resource.getDescription();
        if (!hasText(name)) {
            name = resource.getClass().getSimpleName() + "@" + identityHashCode(resource);
        }

        return name;
    }

}
