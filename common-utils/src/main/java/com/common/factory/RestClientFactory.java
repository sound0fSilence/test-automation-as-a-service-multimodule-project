package com.common.factory;

import com.common.utils.restassuredwrapper.RestClientWrapper;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;

public class RestClientFactory {

    private RestClientFactory() {
    }

    static {
        RestAssured.useRelaxedHTTPSValidation();
    }

    public static RestClientWrapper getClient(String baseUri) {
        return new RestClientWrapper(RestAssured
                .given()
                .baseUri(baseUri)
                .filters(new RequestLoggingFilter(), new ResponseLoggingFilter())
                .contentType(ContentType.JSON));
    }
}
