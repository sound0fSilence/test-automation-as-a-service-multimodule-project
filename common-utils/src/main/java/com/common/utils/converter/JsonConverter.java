package com.common.utils.converter;

import com.common.parser.JsonHandling;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class JsonConverter extends SimpleArgumentConverter {

    @Override
    protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {
        return JsonHandling.fromJson(source.toString(), targetType);
    }
}
