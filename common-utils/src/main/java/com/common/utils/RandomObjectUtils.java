package com.common.utils;

import static java.lang.Integer.parseInt;

import com.github.curiousoddman.rgxgen.RgxGen;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RandomObjectUtils {

    public static String getRandomAlphanumericString(int count) {
        return RandomStringUtils.randomAlphanumeric(count);
    }

    public static String getRandomAlphabeticString(int count) {
        return RandomStringUtils.randomAlphabetic(count);
    }

    public static String getRandomNumericString(int count) {
        return RandomStringUtils.randomNumeric(count);
    }

    public static int getRandomInteger(int count) {
        return parseInt(getRandomNumericString(count));
    }

    public static long getRandomLong(int count) {
        return Long.parseLong(getRandomNumericString(count));
    }

    public static String getRandomStringFromRegex(String regex) {
        return new RgxGen(regex).generate();
    }
}
