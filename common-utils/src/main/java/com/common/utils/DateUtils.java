package com.common.utils;

import static java.lang.String.valueOf;
import static java.time.LocalDate.now;
import static java.time.LocalDate.of;
import static java.time.LocalDate.ofEpochDay;
import static java.time.Month.of;
import static java.time.ZoneId.systemDefault;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.temporal.ChronoUnit.DAYS;

import java.sql.Date;
import java.time.Instant;
import java.util.Optional;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtils {

    public static final String FULL_DATE_WITH_TIME_ZONE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String FULL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String FULL_DATE_MST_FORMAT = "yyyy-MM-dd HH:mm:ss 'MST'";
    public static final String FULL_DATE_MILLISECOND_FORMAT = "YYYY-MM-dd hh:mm:ss.ssssss";
    public static final String CALENDAR_DATE_FORMAT = "MM/DD/YYYY";

    public static String getCurrentDateAsString() {
        return now().format(ISO_LOCAL_DATE);
    }

    public static String getCurrentDateAsRegex() {
        return now().format(ISO_LOCAL_DATE).concat(".*");
    }

    public static String getRandomDateAsString() {
        var startDate = of(1900, of(1), 1);
        var randomDate = ofEpochDay(DAYS.between(startDate, now()));

        return randomDate.format(ISO_LOCAL_DATE);
    }

    public static String convertSqlDateToString(Date dateToConvert) {
        return dateToConvert
                .toLocalDate()
                .format(ISO_LOCAL_DATE);
    }

    public static Date convertStringToSqlDate(java.util.Date dateToConvert) {
        return new Date(dateToConvert.getTime());
    }

    public static String convertSqlDateToString(Optional<Date> dateToConvert) {
        return dateToConvert
                .map(date -> date
                        .toLocalDate()
                        .format(ISO_LOCAL_DATE))
                .orElse(null);
    }

    public static String convertDateToString(java.util.Date dateToConvert) {
        return dateToConvert
                .toInstant()
                .atZone(systemDefault())
                .format(ISO_LOCAL_DATE);
    }

    public static String getDateFromNowMinusYearsAndPlusDaysAsString(int minusYears, int plusDays) {
        return now()
                .minusYears(minusYears)
                .plusDays(plusDays)
                .format(ISO_LOCAL_DATE);
    }

    public static String getDateFromNowPlusYears(int plusYears) {
        return now()
                .plusYears(plusYears)
                .format(ISO_LOCAL_DATE);
    }

    public static String getDateTimeNowInFormat(String format) {
        return DateTime.now().toString(format);
    }

    public static String getDateTimeNowInTimeZoneFormat() {
        return getDateTimeNowInFormat(FULL_DATE_WITH_TIME_ZONE_FORMAT);
    }

    public static String getDateTimeNow() {
        return getDateTimeNowInFormat(FULL_DATE_FORMAT);
    }

    public static String getDateTimeNowInMSTFormat() {
        return getDateTimeNowInFormat(FULL_DATE_MST_FORMAT);
    }

    public static String getDateFromNowInCalendarFormat(int days, int months, int years) {
        return DateTime
                .now()
                .plusDays(days)
                .plusMonths(months)
                .plusYears(years)
                .toString(CALENDAR_DATE_FORMAT);
    }

    public static String getDateTimeNowInSeconds() {
        return valueOf(Instant.now().getEpochSecond());
    }
}
