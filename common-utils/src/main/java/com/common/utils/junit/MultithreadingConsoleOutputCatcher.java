package com.common.utils.junit;

import static java.lang.System.out;
import static java.lang.System.setOut;
import static java.lang.Thread.currentThread;
import static java.util.Optional.of;

import com.common.exceptions.TAFRuntimeException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;

public class MultithreadingConsoleOutputCatcher {

    private MultithreadingConsoleOutputCatcher() {
    }

    private static PrintStream originalStream;
    private static OutputStreamRouter router;
    private static final int BUFFER_CAPACITY_SIZE = 4096;

    public static synchronized void startCatch() {

        if (router == null) {
            originalStream = out;
            router = new OutputStreamRouter(originalStream);
            setOut(new PrintStream(router));
        }
        router.registryThread(currentThread());
    }

    public static String getContent() {

        return getContent(true);
    }

    public static synchronized String getContent(boolean clear) {

        return of(router)
                .orElseThrow(() -> new TAFRuntimeException("Router has not been initialized"))
                .fetchThreadContent(currentThread(), clear);
    }

    public static synchronized void stopCatch() {

        router.unregistryThread(currentThread());
        if (router.getActiveRoutes() == 0) {
            setOut(originalStream);
            originalStream = null;
            router = null;
        }
    }

    static class OutputStreamRouter extends OutputStream {

        private final HashMap<Thread, ByteArrayOutputStream> loggerStreams = new HashMap<>();
        private final OutputStream original;

        public OutputStreamRouter(OutputStream original) {
            this.original = original;
        }

        public void registryThread(Thread thread) {
            if (loggerStreams.containsKey(thread)) {
                throw new TAFRuntimeException();
            }
            loggerStreams.put(thread, new ByteArrayOutputStream(BUFFER_CAPACITY_SIZE));
        }

        public void unregistryThread(Thread thread) {
            if (!loggerStreams.containsKey(thread)) {
                throw new TAFRuntimeException();
            }
            loggerStreams.remove(thread);
        }

        public int getActiveRoutes() {
            return loggerStreams.size();
        }

        public String fetchThreadContent(Thread thread, boolean clear) {
            if (!loggerStreams.containsKey(thread)) {
                throw new TAFRuntimeException();
            }
            var result = loggerStreams.get(thread).toString();
            if (clear) {
                loggerStreams.get(thread).reset();
            }
            return result;
        }

        @Override
        public synchronized void write(int b) throws IOException {
            original.write(b);
            if (loggerStreams.containsKey(currentThread())) {
                loggerStreams.get(currentThread()).write(b);
            }
        }

        @Override
        public synchronized void flush() throws IOException {
            original.flush();
        }

        @Override
        public synchronized void close() throws IOException {
            original.close();
        }
    }
}