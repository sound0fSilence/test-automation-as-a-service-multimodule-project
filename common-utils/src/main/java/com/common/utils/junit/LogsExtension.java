package com.common.utils.junit;

import static com.common.utils.junit.MultithreadingConsoleOutputCatcher.getContent;
import static com.common.utils.junit.MultithreadingConsoleOutputCatcher.startCatch;
import static com.common.utils.junit.MultithreadingConsoleOutputCatcher.stopCatch;
import static io.qameta.allure.Allure.addAttachment;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class LogsExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {

    @Override
    public void beforeTestExecution(ExtensionContext context) {

        startCatch();
    }

    @Override
    public void afterTestExecution(ExtensionContext context) {
        context.getExecutionException().ifPresent(logs -> {
            var logsContent = getContent();
            addAttachment("Test logs", logsContent);
        });
        stopCatch();
    }
}
