package com.common.utils.junit;

import static java.util.Optional.of;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.jupiter.engine.execution.BeforeEachMethodAdapter;
import org.junit.jupiter.engine.extension.ExtensionRegistry;

public class CustomParameterResolver implements BeforeEachMethodAdapter, ParameterResolver {

    private ParameterResolver parameterisedTestParameterResolver;

    @Override
    public void invokeBeforeEachMethod(ExtensionContext context, ExtensionRegistry registry) {
        parameterisedTestParameterResolver = registry.getExtensions(ParameterResolver.class)
                .stream()
                .filter(pr ->
                        pr.getClass().getName()
                                .contains("ParameterizedTestParameterResolver")
                )
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(
                        "ParameterizedTestParameterResolver missed in the registry. Probably it's not a Parameterized Test"));
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext,
            ExtensionContext extensionContext) throws ParameterResolutionException {
        if (isExecutedOnAfterOrBeforeMethod(parameterContext)) {
            var pContext = getMappedContext(parameterContext, extensionContext);

            return parameterisedTestParameterResolver.supportsParameter(pContext, extensionContext);
        }

        return false;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext,
            ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterisedTestParameterResolver.resolveParameter(
                getMappedContext(parameterContext, extensionContext), extensionContext);
    }

    private MappedParameterContext getMappedContext(ParameterContext parameterContext,
            ExtensionContext extensionContext) {
        return new MappedParameterContext(
                parameterContext.getIndex(),
                extensionContext.getRequiredTestMethod().getParameters()[parameterContext.getIndex()],
                of(parameterContext.getTarget()));
    }

    private boolean isExecutedOnAfterOrBeforeMethod(ParameterContext parameterContext) {
        return Arrays.stream(parameterContext.getDeclaringExecutable().getDeclaredAnnotations())
                .anyMatch(this::isAfterEachOrBeforeEachAnnotation);
    }

    private boolean isAfterEachOrBeforeEachAnnotation(Annotation annotation) {
        return annotation.annotationType() == BeforeEach.class
                || annotation.annotationType() == AfterEach.class;
    }
}
