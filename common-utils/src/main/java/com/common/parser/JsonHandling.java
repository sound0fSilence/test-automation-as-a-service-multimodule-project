package com.common.parser;

import com.common.exceptions.TAFRuntimeException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Paths;

public interface JsonHandling {

    static String toJson(final Object o) {
        try {
            return new ObjectMapper().writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new TAFRuntimeException("Error occurs while converting to json: " + e.getMessage());
        }
    }

    static <T> T fromJson(String jsonInput, Class<T> returnType) {
        try {
            return new ObjectMapper().readValue(jsonInput, returnType);
        } catch (JsonProcessingException e) {
            throw new TAFRuntimeException("Error occurs while reading data from json: " + e.getMessage());
        }
    }

    static <T> T readJsonFile(String filePath, Class<T> returnType) {
        try {
            return new ObjectMapper().readValue(Paths.get(filePath).toFile(), returnType);
        } catch (IOException e) {
            throw new TAFRuntimeException("File is not found by path: " + filePath);
        }
    }
}
