package com.common.exceptions;

public class SqlException extends RuntimeException {

    public SqlException(String errorMessage) {
        super(errorMessage);
    }

    public SqlException(String errorMessage, AssertionError error) {
        super(errorMessage, error);
    }
}
