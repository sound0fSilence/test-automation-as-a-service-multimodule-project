package com.common;

import com.common.utils.junit.LogsExtension;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Component
@ExtendWith(value = SpringExtension.class)
@ExtendWith(value = LogsExtension.class)
@SpringBootTest
@DirtiesContext
@Execution(ExecutionMode.CONCURRENT)
public abstract class BaseTest {

}
