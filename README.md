# Test Automation Project

This is a test automation project for Zenith platform.

## Project components

Given test automation project is built with next key frameworks and technologies:

- [Java 17](https://openjdk.org/projects/jdk/17/);
- [Gradle 8.1.1](https://gradle.org/);
- [Spring](https://spring.io/) for bean lifecycle management;
- [REST Assured](https://rest-assured.io/) REST API client library;
- [Kafka](https://kafka.apache.org/) for testing message-driven microservices
- [Project Lombok](https://projectlombok.org/)
- [JUnit 5](https://junit.org/junit5/) testing framework
- [AssertJ](https://assertj.github.io/doc/) for assertions;
- [Allure](https://docs.qameta.io/allure/) reporting framework
- [Jackson](https://github.com/FasterXML/jackson-docs)/[Gson](https://sites.google.com/site/gson/gson-user-guide) for
  serialization/deserialization
- [Awaitility](http://www.awaitility.org/) expectations tool of testing an asynchronous system

## Installation guide

Project usage requires next to be installed:

- Java 17;
- Git;
- Setup [GitLab SSH key](https://docs.gitlab.com/ee/user/ssh.html)
-
Setup [GPG commit signature](https://nrbx.atlassian.net/wiki/spaces/PaaS/pages/58261523/Code+Repositories#Signed-Commits)
- Allure

### Clone project from GitLab:
```
$ cd project_repo
$ git clone git@gitlab.zenith.igzdev.com:zenith-platform/paas-qa/zenith-automation.git
```

# Connection to database

On project there are:

- [PostgreSQL](https://www.postgresql.org/) object-relational database
- [Cassandra](https://cassandra.apache.org/_/index.html) NoSQL database

**TBD**

# Running Tests

To Run tests based on tags specify tags
using [Tag expressions](https://junit.org/junit5/docs/current/user-guide/#running-tests-tag-expressions)
command:

```
$ gradlew clean :<domain-test-project>:runSystemTests -Dspring.profiles.active=<environment> -Dtags=<tags> 
```

Example:

```
$ gradlew clean :domain-c-tests:runSystemTests -Dspring.profiles.active=local -Dtags='(smoke | e2e) & !(disabled | ignore)'
```

As a result, all tests with smoke or e2e tags but without disabled or ignore tags will be executed

To run all tests set tags=all

> **Note**
>
> To run test in parallel mode use junit runner with following properties:
> > junit.platform.output.capture.stdout=true
> > junit.platform.output.capture.stderr=true

## Creating Allure report

To create allure report execute command:

```
$ allure serve <domain-test-project>/build/allure-results
```

Example:

```
$ allure serve domain-c-tests/build/allure-results 
```

## Project structure

### buildSrc

Should contain convention plugins for sharing logic between subprojects

### common-utils

Should contain common classes which are going to be used in domain related tests modules
Should not contain tests

### domain-a-tests,

### domain-b-tests,

### domain-c-tests

Domain related tests modules should contain implementations of automated system tests and classes for related domain
Dependent on common-utils module for using common implementation, uses plugins from buildSrc module.

## Configuring Google code style

1. Launch IntelliJ and go to the **Settings > Preferences…** menu and expand the **Code Style** sub-menu 
underneath **Editor.** Here, you will see a list of supported languages. Select **Java**.
2. Next to the **Scheme** drop-down menu select the gear icon then **Import Scheme > IntelliJ IDEA code style XML**
then select the **_intellij-java-google-style.xml_** file from **Config** folder.
3. Click **OK** or **Apply** for the settings to take effect.