package com.zenith.domainatests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomainATestsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DomainATestsApplication.class, args);
    }

}
