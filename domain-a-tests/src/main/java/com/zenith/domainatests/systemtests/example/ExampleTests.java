package com.zenith.domainatests.systemtests.example;

import com.zenith.accountandcardsdomaintests.systemtests.AbstractTest;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Epic("Demo epic")
public class ExampleTests extends AbstractTest {

    @Description("Demo test")
    @Test
    @Tag("smoke")
    void demoTest() {
        Assertions.assertThat("Demo test").isNotBlank();
    }

}
