package com.zenith.domainatests.systemtests.suites;

import com.zenith.domainatests.systemtests.example.ExampleTests;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses(ExampleTests.class)
@IncludeClassNamePatterns(".*Tests")
public class DomainATestSuite {

}
