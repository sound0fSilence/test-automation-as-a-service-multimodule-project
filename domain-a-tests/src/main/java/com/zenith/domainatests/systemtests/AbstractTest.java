package com.zenith.accountandcardsdomaintests.systemtests;

import com.common.BaseTest;
import com.zenith.domainatests.systemtests.TestConfiguration;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = TestConfiguration.class)
public abstract class AbstractTest extends BaseTest {

}
